package com.suai

import com.sun.xml.internal.fastinfoset.util.StringArray

fun main(args: Array<String>) {

    val encoder = DES()
    print("Enter your keys: ")
    val key = StringArray()
    for (i in 0..2){
        key.add(readLine()!!.toString())
    }
    print ("Encode 0; Decode 1 \n")
    if (readLine()!!.toString() == "0") {
        print("Enter enter file name: ")
        var arr = encoder.loadFromFile(readLine()!!.toString())
        var res = ByteArray(0)

        for (i in 0..2) {
            res = encoder.encode(arr, key[i].toString())
            arr = res
        }
        print("Enter end file name: ")
        val name = encoder.saveToFile(arr, readLine()!!.toString())
    }
    else {
        print("Enter encrypted file name:")
        val name = readLine()!!.toString()
        var res = ByteArray(0)

        val decoder = DES()
        var arr = decoder.loadFromFile(name)
        for (i in 0..2) {
            res = decoder.decode(arr, key[3 - i])
            arr = res
        }
        print("Enter decrypt file name: ")
        decoder.saveToFile(res, readLine()!!.toString())
    }
}
